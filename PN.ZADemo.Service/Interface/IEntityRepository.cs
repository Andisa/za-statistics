﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PN.ZADemo.Service.Interface
{
    public interface IEntityRepository<T> where T : class, new()
    {
        T GetSingle(object Id);
        T Refresh(T entity);

        IQueryable<T> All { get; }
        IQueryable<T> GetAll();
        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IQueryable<T> AllIncludingWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        void Edit(T entity);
        void Add(T entity);
        void Delete(T entity);
        int Save();

        IEnumerable<TDestination> ExecWithStoredProc<TDestination>(string query, params object[] paramters);
    }
}
