﻿using PN.ZADemo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PN.ZADemo.Service.Interface
{
    public interface IZADemoService
    {
        void DeletedArticle(long countryID);
      //  void DeletedDistrict(long districtID);
     //   void DeletedProvince(long provinceID);
        void DeleteIncident(long IncidentID);
        void DeleteIncidentLog(long incidentLogID);
        void DeleteIncidentType(long incidentTypeID);
      //  void DeleteMunicipality(long municipalityID);
     //   void DeleteWard(long wardID);
        void DisableUser(long userId);
        void EnableUser(long userId);
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Country> GetCountries();
        PN.ZADemo.Entity.Country GetCountryByID(long countryID);
       // PN.ZADemo.Entity.District GetDistrictByID(long districtID);
    //    System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.District> GetDistricts();
        PN.ZADemo.Entity.Incident GetIncidentByID(long incidentID);
        PN.ZADemo.Entity.IncidentLog GetIncidentLogByID(long incidentLogID);
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.IncidentLog> GetIncidentLogs();
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Incident> GetIncidents();
        PN.ZADemo.Entity.IncidentType GetIncidentTypeByID(long incidentTypeID);
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.IncidentType> GetIncidentTypes();
     //   System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Municipality> GetMunicipalities();
      //  PN.ZADemo.Entity.Municipality GetMunicipalityByID(long municipalityID);
     //   PN.ZADemo.Entity.Province GetProvinceByID(long provinceID);
     //   System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Province> GetProvinces();
        PN.ZADemo.Entity.Role GetRoleById(long id);
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Role> GetRoles();
        PN.ZADemo.Entity.User GetUserById(long id);
        PN.ZADemo.Entity.User GetUserByUserAuth(string userName, string password);
        PN.ZADemo.Entity.User GetUserByUserName(string userName);
        System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.User> GetUsers();
      //  PN.ZADemo.Entity.Ward GetWardByID(long wardID);
       // System.Collections.Generic.IEnumerable<PN.ZADemo.Entity.Ward> GetWards();
        IEnumerable<Incident> GetIncidents(long WardID);
      IEnumerable<ProvincesCodinate> GetProvincesCodinates();    
        IEnumerable<WardsCordinate> GetWardsCordinates();
        IEnumerable<LocalMunicipalitiesCordinate> GetLocalMunicipalitiesCordinates();
        IEnumerable<DistrictCordinate> GetDistrictCordinates();
        IEnumerable<GetMapData_Result> GetMapData_Result(string ID, string LevelType);
        IEnumerable<IncidentResult> GetIncidentResults(int ProvinceID, int DistrictID, int Municipality, int WardID, int IsClosed, int CategoryID);
        IEnumerable<Status> GetIncidentStatuses();

        IEnumerable<Category> GetCategories();

        void InsertCountry(PN.ZADemo.Entity.Country Country);
     //   void InsertDistrict(PN.ZADemo.Entity.District district);
        void InsertIncident(PN.ZADemo.Entity.Incident incident);
        void InsertIncidentLog(PN.ZADemo.Entity.IncidentLog incidentLog);
        void InsertIncidentType(PN.ZADemo.Entity.IncidentType incidentType);
        void InsertAnonymous(Anonymou anonymous);
      //  void InsertMunicipality(PN.ZADemo.Entity.Municipality municipality);
     ///   void InsertProvince(PN.ZADemo.Entity.Province province);
        void InsertUser(PN.ZADemo.Entity.User user, string passwordHash);
      //  void InsertWard(PN.ZADemo.Entity.Ward ward);
        //void UpdateArticlePreview(PN.ZADemo.Entity.Municipality municipality);
        void UpdateCountry(PN.ZADemo.Entity.Country country);
     //   void UpdateDistrict(PN.ZADemo.Entity.District district);
        void UpdateIncident(PN.ZADemo.Entity.Incident incident);
        void UpdateIncidentLog(PN.ZADemo.Entity.IncidentLog incidentLog);
        void UpdateIncidentType(PN.ZADemo.Entity.IncidentType incidentType);
     //   void UpdateProvince(PN.ZADemo.Entity.Province province);
        void UpdateUser(PN.ZADemo.Entity.User user, string passwordHash = null);
      //  void UpdateWard(PN.ZADemo.Entity.Ward ward);
    }
}
