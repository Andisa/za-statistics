﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PN.ZADemo.Service.Interface;
//using PN.ZADemo.EntityFramework;

using PN.ZADemo.Entity;
using System.Data.SqlClient;

namespace PN.ZADemo.Service.Imp
{
    public class ZADemoService : IZADemoService
    {
        // Todo:
        // - Seperate types into their own services
        private readonly IEntityRepository<Country> _countryRepo;
    
        private readonly IEntityRepository<Incident> _incidentRepo;
        private readonly IEntityRepository<IncidentType> _incidentTypeRepo;
        private readonly IEntityRepository<IncidentLog> _incidentLogRepo;
        private readonly IEntityRepository<User> _userRepo;
        private readonly IEntityRepository<Role> _roleRepo;
        private readonly IEntityRepository<Status> _statusRepo;
        private readonly IEntityRepository<Category> _categoryRepo;



        private readonly IEntityRepository<DistrictCordinate> _districtCordinateRepo;
        private readonly IEntityRepository<LocalMunicipalitiesCordinate> _localMunicipalitiesCordinateRepo;
        private readonly IEntityRepository<ProvincesCodinate> _provincesCodinateRepo;
        private readonly IEntityRepository<WardsCordinate> _wardsCordinateRepo;

        private readonly IEntityRepository<Anonymou> _anonymouRepo;
        public ZADemoService(
            IEntityRepository<Country> countryRepo,
           
            IEntityRepository<Incident> incidentRepo,
        
              IEntityRepository<User> userRepo,

            IEntityRepository<Role> roleRepo,
              IEntityRepository<IncidentType> incidentTypeRepo,
            IEntityRepository<IncidentLog> incidentLogRepo,

            IEntityRepository<DistrictCordinate> districtCordinateRepo,
            IEntityRepository<LocalMunicipalitiesCordinate> localMunicipalitiesCordinateRepo,
                IEntityRepository<ProvincesCodinate> provincesCodinateRepo,
                    IEntityRepository<WardsCordinate> wardsCordinateRepo,
              IEntityRepository<Status> statusRepo,
               IEntityRepository<Category> categoryRepo,
             IEntityRepository<Anonymou> anonymouRepo
            )
        {
            _countryRepo = countryRepo;
          
            _incidentRepo = incidentRepo;
       
            _userRepo = userRepo;
            _roleRepo = roleRepo;

            _incidentLogRepo = incidentLogRepo;
            _incidentTypeRepo = incidentTypeRepo;
         
            _wardsCordinateRepo = wardsCordinateRepo;
            _provincesCodinateRepo = provincesCodinateRepo;
            _localMunicipalitiesCordinateRepo = localMunicipalitiesCordinateRepo;
            _districtCordinateRepo = districtCordinateRepo;
            _statusRepo = statusRepo;
            _categoryRepo = categoryRepo;
            _anonymouRepo = anonymouRepo;

        }

        #region C

        public IEnumerable<Category> GetCategories()
        {
            return _categoryRepo.GetAll();
        }

        public IEnumerable<Country> GetCountries()
        {
            return _countryRepo.GetAll();
        }

        public Country GetCountryByID(long countryID)
        {
            return _countryRepo.GetSingle(countryID);
        }

        public void InsertCountry(Country Country)
        {
            _countryRepo.Add(Country);
            _countryRepo.Save();
        }

        public void UpdateCountry(Country country)
        {
            Country oldCountry = GetCountryByID(country.ID);

            if (oldCountry == null)
                throw new KeyNotFoundException();

            oldCountry.Name = country.Name;
            oldCountry.Description = country.Description;
         


            _countryRepo.Edit(oldCountry);
            _countryRepo.Save();
        }

        public void DeletedArticle(long countryID)
        {
            Country oldCountry = GetCountryByID(countryID);

            if (oldCountry == null)
                throw new KeyNotFoundException();


            oldCountry.IsDeleted = true;


            _countryRepo.Edit(oldCountry);
            _countryRepo.Save();
        }


     
        #endregion


        //#region District
        //public IEnumerable<District> GetDistricts()
        //{
        //    return _districtRepo.GetAllIncluding(a => a.Municipalities);
        //}

        //public District GetDistrictByID(long districtID)
        //{
        //    return _districtRepo.GetSingle(districtID);
        //}

        //public void InsertDistrict(District district)
        //{
        //    _districtRepo.Add(district);
        //    _districtRepo.Save();
        //}

        //public void UpdateDistrict(District district)
        //{
        //    District oldDistrict = GetDistrictByID(district.ID);

        //    if (oldDistrict == null)
        //        throw new KeyNotFoundException();

        //    oldDistrict.Name = district.Name;

           
        //    _districtRepo.Edit(oldDistrict);
        //    _districtRepo.Save();
        //}

        //public void DeletedDistrict(long districtID)
        //{
        //    District oldDistrict = GetDistrictByID(districtID);

        //    if (oldDistrict == null)
        //        throw new KeyNotFoundException();


        //    oldDistrict.IsDeleted = true;


        //    _districtRepo.Edit(oldDistrict);
        //    _districtRepo.Save();
        //}


        //public IEnumerable<Municipality> GetMunicipalities()
        //{
        //    return _municipalityRepo.GetAllIncluding(m=>m.Wards);
        //}

        //public void InsertMunicipality(Municipality municipality)
        //{
        //    _municipalityRepo.Add(municipality);
        //    _municipalityRepo.Save();
        //}

        //public Municipality GetMunicipalityByID(long municipalityID)
        //{
        //    return _municipalityRepo.GetSingle(municipalityID);
        //}


        //public void UpdateArticlePreview(Municipality municipality)
        //{
        //    Municipality oldMunicipality = GetMunicipalityByID(municipality.ID);

        //    if (municipality == null)
        //        throw new KeyNotFoundException();


        //    oldMunicipality.Name = municipality.Name;



        //    _municipalityRepo.Edit(oldMunicipality);
        //    _municipalityRepo.Save();
        //}

        //public void DeleteMunicipality(long municipalityID)
        //{
        //    Municipality oldMunicipality = GetMunicipalityByID(municipalityID);

        //    if (oldMunicipality == null)
        //        throw new KeyNotFoundException();


        //    oldMunicipality.IsDeleted = true;


        //    _municipalityRepo.Edit(oldMunicipality);
        //    _municipalityRepo.Save();
        //}

        //#endregion

        //#region Ward
        //public IEnumerable<Ward> GetWards()
        //{
        //    return _wardRepo.GetAllIncluding(a => a.Incidents);
        //}

        //public Ward GetWardByID(long wardID)
        //{
        //    return _wardRepo.GetSingle(wardID);
        //}

        //public void InsertWard(Ward ward)
        //{
        //    _wardRepo.Add(ward);
        //    _wardRepo.Save();
        //}

        //public void UpdateWard(Ward ward)
        //{
        //    Ward oldWard = GetWardByID(ward.ID);

        //    if (oldWard == null)
        //        throw new KeyNotFoundException();

        //    oldWard.Name = ward.Name;


        //    _wardRepo.Edit(oldWard);
        //    _wardRepo.Save();
        //}

        //public void DeleteWard(long wardID)
        //{
        //    Ward oldWard = GetWardByID(wardID);

        //    if (oldWard == null)
        //        throw new KeyNotFoundException();


        //    oldWard.IsDeleted = true;


        //    _wardRepo.Edit(oldWard);
        //    _wardRepo.Save();
        //}
        //#endregion

        public IEnumerable<Incident> GetIncidents()
        {
            return _incidentRepo.GetAllIncluding(i => i.IncidentType, s => s.Status1, i => i.WardsCordinate, w => w.WardsCordinate.LocalMunicipalitiesCordinate, w => w.WardsCordinate.LocalMunicipalitiesCordinate.DistrictCordinate, w => w.WardsCordinate.LocalMunicipalitiesCordinate.DistrictCordinate.ProvincesCodinate);
        }

        public IEnumerable<Incident> GetIncidents(long WardID)
        {
            return _incidentRepo.AllIncludingWhere(i=>i.WardsCordinate.ID ==WardID,i => i.IncidentType, s => s.Status1);
        }


        public void InsertIncident(Incident incident)
        {
            _incidentRepo.Add(incident);
            _incidentRepo.Save();
            IncidentLog incidentLog = new IncidentLog();
            incidentLog.UserID = incident.UserID;
            incidentLog.Notes = incident.Notes;
            incidentLog.StatusID = incident.StatusID.Value;
            incidentLog.LogDate = DateTime.Now;
            incidentLog.IncidentID = incident.ID;
            InsertIncidentLog(incidentLog);
           
        }

        public Incident GetIncidentByID(long incidentID)
        {
            return _incidentRepo.GetSingle(incidentID);
        }


        public void UpdateIncident(Incident incident)
        {
            Incident oldIncident = GetIncidentByID(incident.ID);

            if (oldIncident == null)
                throw new KeyNotFoundException();


            oldIncident.Notes = incident.Notes;
            oldIncident.Reason = incident.Reason;
            oldIncident.Impact = incident.Impact;
            oldIncident.IncidentTypeID = incident.IncidentTypeID;
            oldIncident.WardID = incident.WardID;
            oldIncident.StatusID = incident.StatusID;
            _incidentRepo.Edit(oldIncident);
            _incidentRepo.Save();
        }


        public void ChangeStatusIncident(Incident incident)
        {
            Incident oldIncident = GetIncidentByID(incident.ID);

            if (oldIncident == null)
                throw new KeyNotFoundException();

            oldIncident.StatusID = incident.StatusID;
            oldIncident.IsClosed = incident.IsClosed;
            oldIncident.CloseDate = incident.CloseDate;
           
            _incidentRepo.Edit(oldIncident);
            _incidentRepo.Save();
        }

        public void DeleteIncident(long IncidentID)
        {
            Incident oldBlogPreview = GetIncidentByID(IncidentID);

            if (oldBlogPreview == null)
                throw new KeyNotFoundException();


            oldBlogPreview.IsDeleted = true;


            _incidentRepo.Edit(oldBlogPreview);
            _incidentRepo.Save();
        }


#region Incident Type     
        public IEnumerable<IncidentType> GetIncidentTypes()
        {
            return _incidentTypeRepo.GetAll();
        }

        public IEnumerable<Status> GetIncidentStatuses()
        {
            return _statusRepo.GetAll();
        }

        public void InsertIncidentType(IncidentType incidentType)
        {
            _incidentTypeRepo.Add(incidentType);
            _incidentTypeRepo.Save();
        }

        public IncidentType GetIncidentTypeByID(long incidentTypeID)
        {
            return _incidentTypeRepo.GetSingle(incidentTypeID);
        }


        public void UpdateIncidentType(IncidentType incidentType)
        {
            IncidentType oldIncidentType = GetIncidentTypeByID(incidentType.ID);

            if (oldIncidentType == null)
                throw new KeyNotFoundException();


            oldIncidentType.Type = incidentType.Type;
           

            _incidentTypeRepo.Edit(oldIncidentType);
            _incidentTypeRepo.Save();
        }

        public void DeleteIncidentType(long incidentTypeID)
        {
            IncidentType oldType = GetIncidentTypeByID(incidentTypeID);

            if (oldType == null)
                throw new KeyNotFoundException();


         //   oldType. = true;


            _incidentTypeRepo.Edit(oldType);
            _incidentTypeRepo.Save();
        }

        public void InsertAnonymous(Anonymou anonymous)
        {
            _anonymouRepo.Add(anonymous);
            _anonymouRepo.Save();
        }
        #endregion


        #region Incident Log
        public IEnumerable<IncidentLog> GetIncidentLogs()
        {
            return _incidentLogRepo.GetAllIncluding(t=>t.Status, i=>i.Incident);
        }

        public void InsertIncidentLog(IncidentLog incidentLog)
        {
            _incidentLogRepo.Add(incidentLog);
            _incidentLogRepo.Save();

            var status = GetIncidentStatuses().Where(i => i.ID == incidentLog.StatusID).FirstOrDefault();
           
                Incident incident = new Incident();
                if (string.Equals(status.IncidentStatus, "Close"))
                {
                incident.IsClosed =true;
                incident.CloseDate = DateTime.Now;
                }
                incident.ID = incidentLog.IncidentID;
                incident.StatusID = incidentLog.StatusID;
                ChangeStatusIncident(incident);
           

        }

        public IncidentLog GetIncidentLogByID(long incidentLogID)
        {
            return _incidentLogRepo.GetSingle(incidentLogID);
        }


        public void UpdateIncidentLog(IncidentLog incidentLog)
        {
            IncidentLog oldIncidentLog = GetIncidentLogByID(incidentLog.ID);

            if (oldIncidentLog == null)
                throw new KeyNotFoundException();


            oldIncidentLog.Notes = incidentLog.Notes;


            _incidentLogRepo.Edit(oldIncidentLog);
            _incidentLogRepo.Save();
        }

        public void DeleteIncidentLog(long incidentLogID)
        {
            IncidentLog oldType = GetIncidentLogByID(incidentLogID);

            if (oldType == null)
                throw new KeyNotFoundException();


            //   oldType. = true;


            _incidentLogRepo.Edit(oldType);
            _incidentLogRepo.Save();
        }
        #endregion
        #region Users

        public User GetUserById(long id)
        {
            return _userRepo.AllIncludingWhere(user => user.ID == id, user => user.Roles).FirstOrDefault();
        }

        public User GetUserByUserName(string userName)
        {
            return _userRepo.AllIncludingWhere(
              user => user.UserName == userName,
              user => user.Roles

            ).FirstOrDefault();
        }

        public User GetUserByUserAuth(string userName, string password)
        {

            return _userRepo.AllIncludingWhere(
              user => user.UserName == userName && user.PasswordHash == password,
              user => user.Roles

            ).FirstOrDefault();
        }

        public void InsertUser(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordHash;
           // user.IsActive = false;

            long roleId = -1;
            if (long.TryParse(user.StrRoles, out roleId))
            {
                Role role;
                if ((role = GetRoleById(roleId)) != null)
                {
                    user.Roles.Add(role);
                }
            }

            _userRepo.Add(user);
            _userRepo.Save();
        }

        public void UpdateUser(User user, string passwordHash = null)
        {

            User oldUser = GetUserById(user.ID);
            if (oldUser != null)
            {
                oldUser.FirstName = user.FirstName;
              
                oldUser.Email = user.Email;

                if (!String.IsNullOrEmpty(passwordHash))
                {
                    oldUser.PasswordHash = oldUser.PasswordSalt = passwordHash;
                }

                long newRoleId = -1;
                Role currentRole = oldUser.Roles.FirstOrDefault();

                if (long.TryParse(user.StrRoles, out newRoleId))
                {
                    if (currentRole.ID != newRoleId)
                    {
                        Role newRole = GetRoleById(newRoleId);

                        if (newRole != null)
                        {
                            oldUser.Roles.Remove(currentRole);
                            oldUser.Roles.Add(newRole);
                        }
                    }
                }

                _userRepo.Edit(oldUser);
                _userRepo.Save();
            }
        }

        public void DisableUser(long userId)
        {
            User user = GetUserById(userId);
            user.IsActive = false;

            _userRepo.Edit(user);
            _userRepo.Save();
        }

        public void EnableUser(long userId)
        {
            User user = GetUserById(userId);
            user.IsActive = true;

            _userRepo.Edit(user);
            _userRepo.Save();
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepo.GetAllIncluding(

              user => user.Roles
            );
        }



        public Role GetRoleById(long id)
        {
            return _roleRepo.GetSingle(id);
        }



        public IEnumerable<Role> GetRoles()
        {
            return _roleRepo.GetAll();
        }



        #endregion

        #region Map

        
        public IEnumerable<ProvincesCodinate> GetProvincesCodinates()
        {
            return _provincesCodinateRepo.GetAll();
        }



         public IEnumerable<WardsCordinate> GetWardsCordinates()
        {
            return _wardsCordinateRepo.GetAllIncluding(w=>w.LocalMunicipalitiesCordinate.DistrictCordinate);
        }

         public IEnumerable<LocalMunicipalitiesCordinate> GetLocalMunicipalitiesCordinates()
        {
            return _localMunicipalitiesCordinateRepo.GetAllIncluding(m=>m.DistrictCordinate.ProvincesCodinate);
        }

        
         public IEnumerable<DistrictCordinate> GetDistrictCordinates()
        {
            return _districtCordinateRepo.GetAll();
        }


         public IEnumerable<GetMapData_Result> GetMapData_Result(string ID, string LevelType)
         {
             return _provincesCodinateRepo.ExecWithStoredProc<GetMapData_Result>(
               "GetMapData @ID, @LevelType",
               new SqlParameter("ID", System.Data.SqlDbType.VarChar) { Value = ID },
                new SqlParameter("LevelType", System.Data.SqlDbType.VarChar) { Value = LevelType }
             );
         }

    

         public IEnumerable<IncidentResult> GetIncidentResults(int ProvinceID,	int DistrictID,	int Municipality,	int WardID ,	int IsClosed, int CategoryID)
         {
             return _incidentRepo.ExecWithStoredProc<IncidentResult>(
               "GetIncidents @ProvinceID, @DistrictID, @Municipality,@WardID, @IsClosed",
               new SqlParameter("ProvinceID", System.Data.SqlDbType.Int) { Value = ProvinceID },
                new SqlParameter("DistrictID", System.Data.SqlDbType.Int) { Value = DistrictID },
                    new SqlParameter("Municipality", System.Data.SqlDbType.Int) { Value = Municipality },
                new SqlParameter("WardID", System.Data.SqlDbType.Int) { Value = WardID },
                        new SqlParameter("IsClosed", System.Data.SqlDbType.Int) { Value = IsClosed },
                        new SqlParameter("CategoryID", System.Data.SqlDbType.Int) { Value = CategoryID }

             );
         }
        #endregion


    }
}
