﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PN.ZADemo.Service.Interface;

namespace PN.ZADemo.Service.Imp
{
    public class EntityRepository<T> : IEntityRepository<T>
      where T : class, new()
    {

        protected readonly DbContext _entityContext;
        protected DbSet<T> _dbSet;

        public EntityRepository(DbContext context)
        {
            this._entityContext = context;
            this._dbSet = _entityContext.Set<T>();
        }

        public virtual T GetSingle(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual IQueryable<T> All
        {
            get { return GetAll(); }
        }

        public virtual IQueryable<T> GetAll()
        {
            return _dbSet;
        }

        public virtual IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet;

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }
        public virtual IQueryable<T> AllIncludingWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = GetAllIncluding(includeProperties);

            return query.Where(predicate);
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public virtual void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = _entityContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = _entityContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Added;
        }

        public virtual T Refresh(T entity)
        {
            _entityContext.Entry<T>(entity).Reload();
            return entity;
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = _entityContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public virtual int Save()
        {
            return _entityContext.SaveChanges();
        }

        public IEnumerable<TDestination> ExecWithStoredProc<TDestination>(string query, params object[] paramters)
        {
            return _entityContext.Database.SqlQuery<TDestination>(query, paramters);
        }

    }
}
