﻿

$(document).ready(function () {
       
        $('#txtUserName, #txtPassword').on("keyup", action);
       
        $("#msg").empty();
    });

function action() {
    if ($('#txtUserName').val() !=='' && $('#txtPassword').val() !=='') {
        $('#btnLogin').prop("disabled", false);
    } else {
        $('#btnLogin').prop("disabled", true);
       
    }
}
    

   
function Login() {
    $('#busy').prop("disabled", false);
    var url= '@Url.Action("Login")';
    var uname = $("#txtUserName").val();
    var pass = $("#txtPassword").val();
        
    var user = {
            
        UserName: uname,
        Password: pass
    };
    $.ajax({
        type:"POST",
        url:url,
        data: JSON.stringify(user),
        datatype:"JSON",
        contentType:"application/json; charset=utf-8",
        success: function (returndata) {
            if (returndata.ok) {                   
                $('#busy').prop("disabled", true);
                $('#divmsg').prop("hidden", false);
                $("#msg").empty();
                $("#msg").append(returndata.message);
            }
            else {
                $('#busy').prop("disabled", true);
                $("#msg").empty();
                $('#divmsg').prop("hidden", false);
                   
                $("#msg").append(returndata.message);
            }

        }
    }
    );

}
    
    


