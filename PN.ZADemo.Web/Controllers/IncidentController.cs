﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PN.ZADemo.Web.Models;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.Entity;

using System.Net.Mail;
using System.Net;
using System.Text;

namespace PN.ZADemo.Web.Controllers
{
    public class IncidentController : Controller
    {



        private readonly IZADemoService _IZADemoService;
        public IncidentController() { }
        public IncidentController(IZADemoService zaSvc)
        {
            _IZADemoService = zaSvc;
        }



        [Authorize]
        public ActionResult Incidents(string WardNo = "0")
        {

            string username = System.Web.HttpContext.Current.User.Identity.Name;
            User user = _IZADemoService.GetUserByUserName(username);
            long userrole = user.Roles.FirstOrDefault().ID;
            ViewBag.Role = userrole;
            ViewBag.CurrID = user.LevelID;
            ViewBag.WardID = WardNo;

            return View();
        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="Anonymous"></param>
      /// <returns></returns>
  //[Authorize]
        public ActionResult ReportIncident(string Anonymous = "No")
        {
            if (Anonymous == "No")
            {
                string username = System.Web.HttpContext.Current.User.Identity.Name;
                User user = _IZADemoService.GetUserByUserName(username);
                long userrole = user.Roles.FirstOrDefault().ID;
                ViewBag.Role = userrole;
                ViewBag.CurrID = user.LevelID;
                ViewBag.WardID = user.LevelID;
            }

         else   if (Anonymous == "Yes")
            {
                 ViewBag.Role = 2;
                ViewBag.CurrID =0;
                ViewBag.WardID = 0;
            }

            else
            {
                 ViewBag.Role = 0;
                ViewBag.CurrID = 0;
                ViewBag.WardID = 0;
            }
            ViewBag.IncidentStatuses = _IZADemoService.GetIncidentStatuses();
            ViewBag.IncidentTypes = _IZADemoService.GetIncidentTypes();
            ViewBag.Categories = _IZADemoService.GetCategories();
            //  ViewBag.Wards = _IZADemoService.GetWards().Where(w => w.MunicipalityID == user.LevelID).OrderBy(o => o.Name);
            return View();
        }


        public ActionResult ReportIncidentThankYou(string session)
        {
            long incidentID = Providers.Helper.DecoderLongID(session);
            ViewBag.IID = incidentID;
            return View();
        }

        [Authorize]
        public ActionResult EditIncident(string id)
        {
            long incidentID = Providers.Helper.DecoderLongID(id);
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            User user = _IZADemoService.GetUserByUserName(username);
            long userrole = user.Roles.FirstOrDefault().ID;
            ViewBag.Role = userrole;
            ViewBag.CurrID = user.LevelID;
            ViewBag.WardID = user.LevelID;
            ViewBag.IncidentStatuses = _IZADemoService.GetIncidentStatuses();

            ViewBag.IncidentTypes = _IZADemoService.GetIncidentTypes();

            var incident = _IZADemoService.GetIncidentByID(incidentID);
            //  ViewBag.Wards = _IZADemoService.GetWards().Where(w => w.MunicipalityID == user.LevelID).OrderBy(o => o.Name);
            return View(incident);
        }
        

        [Authorize]
        public ActionResult WardIncidentdetails(string id)
        {
           /// long incidentID = Providers.Helper.DecoderLongID(id);

            ViewBag.IncidentID = id;
            return View();
        }


          [Authorize]
        public ActionResult IncidentDetails(string id)
        {
           /// long incidentID = Providers.Helper.DecoderLongID(id);

            ViewBag.IncidentID = id;
            return View();
        }


        [Authorize]
        public ActionResult LogIncidentStatus(string id)
        {
            long incidentID = Providers.Helper.DecoderLongID(id);
            string username = System.Web.HttpContext.Current.User.Identity.Name;
            User user = _IZADemoService.GetUserByUserName(username);
            long userrole = user.Roles.FirstOrDefault().ID;
            ViewBag.Role = userrole;
            ViewBag.CurrID = user.LevelID;
            ViewBag.WardID = user.LevelID;
            ViewBag.IncidentStatuses = _IZADemoService.GetIncidentStatuses();

            ViewBag.IncidentTypes = _IZADemoService.GetIncidentTypes();

            var incident = _IZADemoService.GetIncidentByID(incidentID);
            //  ViewBag.Wards = _IZADemoService.GetWards().Where(w => w.MunicipalityID == user.LevelID).OrderBy(o => o.Name);
            return View(incident);
        }



        [Authorize]
        [HttpPost]
        public JsonResult GetIncidents(int level = 0, int levelID = 0, int provinceID = 0, int districtID = 0, int municipalityID = 0, int wardID = 0, int isClosed = 0,  int categoryID =0,int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int count = 0;


                IEnumerable<IncidentResult> Incidents = null;

                switch (level)
                {

                    case 2:

                        break;
                    case 3:
                        provinceID = levelID;
                        break;
                    case 4:
                        districtID = levelID;
                        break;
                    case 5:
                        municipalityID = levelID;
                        break;



                    default:
                        break;
                }

                count = _IZADemoService.GetIncidentResults(provinceID, districtID, municipalityID, wardID, isClosed, categoryID).Count();

                if (count > 0)
                    Incidents = _IZADemoService.GetIncidentResults(provinceID, districtID, municipalityID, wardID, isClosed, categoryID).OrderByDescending(n => n.IncidentID).Skip(jtStartIndex).Take(jtPageSize).ToList();
                else
                {
                    Incidents = _IZADemoService.GetIncidentResults(provinceID, districtID, municipalityID, wardID, isClosed, categoryID).OrderByDescending(n => n.IncidentID).ToList();
                }




                switch (jtSorting)
                {
                    case "Date DESC":
                        Incidents = Incidents.OrderByDescending(s => s.DateOccurred);
                        break;

                    default:  // Name ascending 
                        Incidents = Incidents.OrderBy(s => s.IncidentID);
                        break;
                }

                return Json(new { Result = "OK", ok = true, Records = Incidents.Select(r => new { ID = r.IncidentID, Type = r.IncidentType,Impact = r.IncidentType,r.Category, WardID = r.WardID, Ward = r.WardNo, Municipality = r.MunicipalityName, District = r.DistrictName, Province = r.ProvinceName, Status = r.IncidentStatus, DateOccurred = r.DateOccurred.ToString("dd-MMM-yyyy"), DateClosed = r.CloseDate ==null? "Open":r.CloseDate.Value.ToString("dd-MMM-yyyy") }), TotalRecordCount = count });

            }


            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
            }


        }

        //[Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitIncident()
        {
            try
            {
                long  userID = 0;
                string impact = Request.Form["Impact"].ToString();
                string notes = Request.Form["Notes"].ToString();
                string status = Request.Form["Status"].ToString();
                int wardID = Convert.ToInt32(Request.Form["WardID"].ToString());
                int incidentTypeID = Convert.ToInt32(Request.Form["IncidentTypeID"].ToString());
                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string username = System.Web.HttpContext.Current.User.Identity.Name;
                    User user = _IZADemoService.GetUserByUserName(username);
                    userID =user.ID;
                }
                Incident incident = new Incident();
                incident.DateOccurred = DateTime.Now;
                incident.Impact = impact;
                incident.Notes = notes;
                incident.WardID = wardID;
                incident.IncidentTypeID = incidentTypeID;
                incident.Status = status;
                incident.StatusID = Convert.ToInt32(status);
                incident.TimeSatmp = DateTime.Now;
                incident.IsClosed = false;
                incident.IsDeleted = false;
                incident.UserID = userID;
                incident.Reason = incidentTypeID.ToString();

                _IZADemoService.InsertIncident(incident);
                string iID = Providers.Helper.EncodeTo64(incident.ID.ToString());
                return Json(new { ok = true, message = "Incident sucessfully submited!", id = iID, uID = userID });

            }

            catch (Exception ex)
            {
                return Json(new { ok = false, message = ex.Message, id = 0 });
            }

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitAnonymous(Anonymou Anonymous)
        {
            try
            {
                Anonymous.TimeStamp = DateTime.Now;
                _IZADemoService.InsertAnonymous(Anonymous);
            
                return Json(new { ok = true, message = "sucessfully submited!" });

            }

            catch (Exception ex)
            {
                return Json(new { ok = false, message = ex.Message, id = 0 });
            }

        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitIncidentChanges()
        {
            try
            {
                string impact = Request.Form["Impact"].ToString();
                string notes = Request.Form["Notes"].ToString();
                string status = Request.Form["Status"].ToString();
                int wardID = Convert.ToInt32(Request.Form["WardID"].ToString());
                int incidentTypeID = Convert.ToInt32(Request.Form["IncidentTypeID"].ToString());
                long ID = Convert.ToInt64(Request.Form["ID"].ToString());
                // string username = System.Web.HttpContext.Current.User.Identity.Name;
                // User user = _IZADemoService.GetUserByUserName(username);
                Incident incident = new Incident();
                // incident.DateOccurred = DateTime.Now;
                incident.Impact = impact;
                incident.Notes = notes;
                incident.WardID = wardID;
                incident.IncidentTypeID = incidentTypeID;
                incident.Status = status;
                incident.StatusID = Convert.ToInt32(status);
                // incident.TimeSatmp = DateTime.Now;
                incident.IsClosed = false;
                incident.IsDeleted = false;
                // incident.UserID = user.ID;
                incident.Reason = incidentTypeID.ToString();
                incident.ID = ID;
                _IZADemoService.UpdateIncident(incident);

                return Json(new { ok = true, message = "Incident sucessfully updated!", id = incident.ID });

            }

            catch (Exception ex)
            {
                return Json(new { ok = false, message = ex.Message, id = 0 });
            }

        }





        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitLogIncidentStatus()
        {
            try
            {

                string notes = Request.Form["Notes"].ToString();
                string status = Request.Form["Status"].ToString();

                long ID = Convert.ToInt64(Request.Form["ID"].ToString());
                string username = System.Web.HttpContext.Current.User.Identity.Name;
                User user = _IZADemoService.GetUserByUserName(username);
                IncidentLog incidentLog = new IncidentLog();
                incidentLog.Notes = notes;
                incidentLog.StatusID = Convert.ToInt32(status);
                incidentLog.UserID = user.ID;
                incidentLog.IncidentID = ID;
               
                incidentLog.LogDate = DateTime.Now;
                _IZADemoService.InsertIncidentLog(incidentLog);
             
                return Json(new { ok = true, message = "Incident log sucessfully saved!", id = incidentLog.ID });

            }

            catch (Exception ex)
            {
                return Json(new { ok = false, message = ex.Message, id = 0 });
            }

        }



        [Authorize]
        [HttpPost]
        public JsonResult GetIncidentDetails(string ID)
        {
            try
            {
                long incidentID = Providers.Helper.DecoderLongID(ID);

             
                   var incident = _IZADemoService.GetIncidents().Where(i=>i.ID==incidentID).FirstOrDefault();
                   var incidentLogs = _IZADemoService.GetIncidentLogs().Where(i=>i.IncidentID==incidentID);



             
                return Json(new { Result = "OK", ok = true, WardName = incident.WardsCordinate.MUNICNAME +" - Ward No:" + incident.WardsCordinate.WARDNO, IncidentType = incident.IncidentType.Type, IncidentStatus = incident.Status1.IncidentStatus, Date = incident.DateOccurred.ToString("dd-MMM-yyyy"), Records = incidentLogs.Select(r => new { ID = r.ID, Status = r.Status.IncidentStatus, LogDate = r.LogDate.ToString("dd-MMM-yyyy"), Notes =r.Notes }), TotalRecordCount = 1 });

            }


            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
            }


        }



        [Authorize]
        [HttpPost]
        public JsonResult GetWardIncidentDetails(string ID)
        {
            try
            {
                long wardID = Providers.Helper.DecoderLongID(ID);


                var incidents = _IZADemoService.GetIncidents(wardID);

                var ward = _IZADemoService.GetWardsCordinates().Where(i => i.ID == wardID).FirstOrDefault();

                return Json(new { Result = "OK", ok = true, WardName = ward.MUNICNAME + " - Ward No:" + ward.WARDNO, Province = ward.PROVINCE, District = ward.LocalMunicipalitiesCordinate.DistrictCordinate.DISTRICT, NumberOfincidents = incidents.Count().ToString(), Records = incidents.Select(r => new { ID = r.ID, Status = r.Status1.IncidentStatus, LogDate = r.DateOccurred.ToString("dd-MMM-yyyy"), Notes = r.Notes }), TotalRecordCount = 1 });

            }


            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
            }


        }


        [Authorize]
        [HttpPost]
        public JsonResult GetDistrictIncidentDetails(string ID)
        {
            try
            {
                long munID = Providers.Helper.DecoderLongID(ID);


                var incidents = _IZADemoService.GetIncidents().Where(i => i.WardsCordinate.MunicipalityID == munID);

                var ward = _IZADemoService.GetLocalMunicipalitiesCordinates().Where(i => i.ID == munID).FirstOrDefault();

                return Json(new { Result = "OK", ok = true, WardName = ward.MUNICNAME , Province = ward.PROVINCE, District = ward.DistrictCordinate.DISTRICT, NumberOfincidents = incidents.Count().ToString(), Records = incidents.Select(r => new { ID = r.ID, Status = r.Status1.IncidentStatus, LogDate = r.DateOccurred.ToString("dd-MMM-yyyy"), Notes = r.Notes }), TotalRecordCount = 1 });

            }


            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
            }


        }
    }
}
