﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PN.ZADemo.Web.Models;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.Entity;

using System.Net.Mail;
using System.Net;
using System.Text;

namespace PN.ZADemo.Web.Controllers
{
  public class DashboardController : Controller {



      private readonly IZADemoService _IZADemoService;
    public DashboardController() { }
    public DashboardController(IZADemoService zaSvc)
    {
        _IZADemoService = zaSvc;
    }



 [Authorize]
    public ActionResult Main()
    {

        string username = System.Web.HttpContext.Current.User.Identity.Name;
        User user = _IZADemoService.GetUserByUserName(username);
        long userrole = user.Roles.FirstOrDefault().ID;
        ViewBag.Role = userrole;
        ViewBag.CurrID = user.LevelID;
        ViewBag.CurrCode = "";

        switch (userrole)
        {

            case 2:

                break;
            case 3:
              var province =  _IZADemoService.GetProvincesCodinates().Where(p=>p.ID==user.LevelID).FirstOrDefault();
              ViewBag.CurrCode = province.PROVINCE;
              ViewBag.Province = province.PROVINCE;
                break;
            case 4:
                var district = _IZADemoService.GetDistrictCordinates().Where(d => d.ID == user.LevelID).FirstOrDefault();
                ViewBag.CurrCode = district.DISTRICT;
                ViewBag.Province = district.PROVNAME;
                break;
            case 5:
               var municipality = _IZADemoService.GetLocalMunicipalitiesCordinates().Where(m => m.ID == user.LevelID).FirstOrDefault();
               ViewBag.CurrCode = municipality.CAT_B;//ID = levelID;
               ViewBag.Province = municipality.DistrictCordinate.ProvincesCodinate.PROVINCE;
                break;



            default:
                break;
        }
        //ViewBag.WardID = WardNo;
        return View();
    }


       [Authorize]
    public ActionResult Index()
    {

        string username = System.Web.HttpContext.Current.User.Identity.Name;
       
        return View();
    }


    [HttpPost]
    public JsonResult GetProvinces()
    {
        try
        {
            int count = 0;

            IEnumerable<GetMapData_Result> provinces = null;
            provinces = _IZADemoService.GetMapData_Result(" ","Province");

            return Json(new { Result = "OK", ok = true, Records = provinces.Select(r => new { id = r.Name, fillColor = r.ColorCode, strokeColor = r.BorderColor }), TotalRecordCount = count });
        }

        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }


    }

    [HttpPost]
    public JsonResult GetDistricts(string Province)
    {
        try
        {
            int count = 0;

            IEnumerable<GetMapData_Result> provinces = null;
            provinces = _IZADemoService.GetMapData_Result(Province, "District");

            return Json(new { Result = "OK", ok = true, Records = provinces.Select(r => new { id = r.Name, fillColor = r.ColorCode, strokeColor = r.BorderColor }), TotalRecordCount = count });
        }

        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }



    }

    [HttpPost]
    public JsonResult GetMunicipalities(string District)
    {
        try
        {
            int count = 0;

            IEnumerable<GetMapData_Result> Municipalities = null;
            Municipalities = _IZADemoService.GetMapData_Result(District, "Municipality");

            return Json(new { Result = "OK", ok = true, Records = Municipalities.Select(r => new { id = r.Name, fillColor = r.ColorCode, strokeColor = r.BorderColor }), TotalRecordCount = count });
        }

        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }




    }



    [HttpPost]
    public JsonResult GetWards(string Municipality)
    {
        try
        {
            int count = 0;
           

                IEnumerable<GetMapData_Result> Municipalities = null;
                Municipalities = _IZADemoService.GetMapData_Result(Municipality, "Ward");

                return Json(new { Result = "OK", ok = true, Records = Municipalities.Select(r => new { id = r.Name, fillColor = r.ColorCode, strokeColor = r.BorderColor, html = "<strong>" + r.ParentName +  "</strong><br><a href='../Incident/Incidents?WardNo=" + r.ID + "' class='goto-login pull-left'> View Detailed Report</a>!" }), TotalRecordCount = count });
            }

            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
            }


        }





    [HttpPost]
    public JsonResult GetWardsCordinates(int wardID)
    {
        try
        {
            int count = 0;

            IEnumerable<WardsCordinate> WardsCordinates = null;
            WardsCordinates = _IZADemoService.GetWardsCordinates().Where(p => p.ID == wardID).OrderByDescending(n => n.WARDNO).ToList();


            return Json(new { Result = "OK", ok = true, Records = WardsCordinates.Select(r => new { ID = r.ID, Name = r.MUNICNAME, Impact = r.WARDNO }), TotalRecordCount = count });

        }


        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }


    }

 
  }
}
