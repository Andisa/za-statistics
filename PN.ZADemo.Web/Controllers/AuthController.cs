﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PN.ZADemo.Web.Models;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.Entity;

using System.Net.Mail;
using System.Net;
using System.Text;
using PN.ZADemo.Web.Models;

namespace PN.ZADemo.Web.Controllers
{
  public class AuthController : Controller {



      private readonly IZADemoService _IZADemoService;
    public AuthController() { }
    public AuthController(IZADemoService zaSvc)
    {
        _IZADemoService = zaSvc;
    }

    public ActionResult Index() {
      return View();
    }

    public ActionResult Login()
    {
        ViewBag.Roles = _IZADemoService.GetRoles().OrderBy(u => u.Role1).ToList();
        //ViewBag.Provinces = _IZADemoService.GetProvincesCodinates().OrderBy(u => u.PROVINCE).ToList();
        return View();
    }


    public ActionResult Main()
    {
        return View();
    }


    public ActionResult Logout()
    {
        FormsAuthentication.SignOut();

        return View();
    }



    [HttpPost]
 
    public ActionResult Login(LoginModel loginModel)
    {
        if (ModelState.IsValid)
        {
            string passHash = FormsAuthentication.HashPasswordForStoringInConfigFile(loginModel.Password, "SHA1");
            User user = _IZADemoService.GetUserByUserAuth(loginModel.UserName, passHash);

            if (Membership.ValidateUser(loginModel.UserName, loginModel.Password))
            {
                if(loginModel.RememberMe)
                    FormsAuthentication.SetAuthCookie(loginModel.UserName, true);


                bool isAdmin = false;// user.Roles.Any(u => u.Role1 == "Admin");

                return Json(new { IsAdmin = isAdmin, messaged = "Logged: " + loginModel.UserName });
            }
        }

        Response.StatusCode = 400;
        return null;
    }


    #region User

    public ActionResult CreateUser()
    {
        var users = _IZADemoService.GetRoles().Where(u => u.Role1 == "Admin");
        if (users.Count() < 1)
        {

        }

        ViewBag.Roles = _IZADemoService.GetRoles().OrderBy(u => u.Role1).ToList();

        return View();

    }

    [HttpPost]
  
    public ActionResult Register(User user)
    {
        try
        {

            var users = _IZADemoService.GetUsers().Where(u => u.UserName == user.UserName);
            if (users.Count() > 0)
            {
                return Json(new { ok = false, message = "Username already exists!" });
            }
            else
            {

                string passHash = HashPassword(user.PasswordHash);
                user.PasswordHash = passHash;
                user.PasswordSalt = passHash;
                user.IsActive = true;
                user.IsApproved = true;
                user.IsLocked = false;
                user.CreatedDate = DateTime.Now;
                user.AccountType = Convert.ToInt32(user.StrRoles);
                _IZADemoService.InsertUser(user, passHash);

                FormsAuthentication.SetAuthCookie(user.Email, false);


                    bool isAdmin = false;// user.Roles.Any(u => u.Role1 == "Admin");

                    return Json(new {ok=true, IsAdmin = isAdmin, logged= true, messaged = "Logged: " + user.Email });
                
               

            }


        }

        catch (Exception ex)
        {
            return Json(new { ok = false, message = ex.Message, logged =false });
        }

    }

    private string HashPassword(string password)
    {
        if (!String.IsNullOrEmpty(password))
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");

        return null;
    }
    #endregion




    [HttpPost]
    public JsonResult GetProvinces()
    {
        try
        {
            int count = 0;

            IEnumerable<ProvincesCodinate> Provinces = null;       
            Provinces = _IZADemoService.GetProvincesCodinates().OrderByDescending(n => n.PROVINCE).ToList();


            return Json(new { Result = "OK", ok = true, Records = Provinces.Select(r => new { ID = r.ID, Name = r.PROVINCE, Code = r.CODE }), TotalRecordCount = count });

        }


        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }


    }



    [HttpPost]
    public JsonResult GetDistricts(int ProvinceID)
    {
        try
        {
            int count = 0;
            IEnumerable<DistrictCordinate> Districts = null;
            Districts = _IZADemoService.GetDistrictCordinates().Where(p => p.ProvinceID == ProvinceID).OrderByDescending(n => n.MUNICNAME).ToList();

            return Json(new { Result = "OK", ok = true, Records = Districts.Select(r => new { ID = r.ID, Name = r.MUNICNAME, Code = r.DISTRICT }), TotalRecordCount = count });
        }

        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }

    }


    [HttpPost]
    public JsonResult GetLocalMunicipalities(int DistrictID)
    {
        try
        {
            int count = 0;

            IEnumerable<LocalMunicipalitiesCordinate> LocalMunicipalitiesCordinates = null;
            LocalMunicipalitiesCordinates = _IZADemoService.GetLocalMunicipalitiesCordinates().Where(p => p.DistrictID == DistrictID).OrderByDescending(n => n.MAP_TITLE).ToList();


            return Json(new { Result = "OK", ok = true, Records = LocalMunicipalitiesCordinates.Select(r => new { ID = r.ID, Name = r.MAP_TITLE, Code = r.DISTRICT }), TotalRecordCount = count });

        }


        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }


    }


    [HttpPost]
    public JsonResult GetWards(int MuniID)
    {
        try
        {
            int count = 0;

            IEnumerable<WardsCordinate> WardsCordinates = null;
            WardsCordinates = _IZADemoService.GetWardsCordinates().Where(p => p.MunicipalityID == MuniID).OrderByDescending(n => n.WARDNO).ToList();


            return Json(new { Result = "OK", ok = true, Records = WardsCordinates.Select(r => new { ID = r.ID, Name = r.WARDNO, Impact = r.WARDNO }), TotalRecordCount = count });

        }


        catch (Exception ex)
        {
            return Json(new { Result = "ERROR", ok = false, Message = ex.Message });
        }


    }





  }
}
