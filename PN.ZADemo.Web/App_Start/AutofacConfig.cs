﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using PN.ZADemo.Entity;
using PN.ZADemo.Service;
using PN.ZADemo.EntityFramework;
using PN.ZADemo.Service.Imp;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.Entity;

namespace PN.ZADemo.Web
{
  public class AutofacConfig {
    public static void Initialize(HttpConfiguration config) {
      Initialize(config, RegisterServices(new ContainerBuilder()));
    }

    public static void Initialize(HttpConfiguration config, IContainer container) {
      config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
      DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
    }

    public static IContainer RegisterServices(ContainerBuilder builder) {
      Assembly executingAssembly = Assembly.GetExecutingAssembly();

      builder.RegisterApiControllers(executingAssembly);
      builder.RegisterControllers(executingAssembly);

      builder.RegisterType<ZADemoEntities>()
             .As<DbContext>()
             .InstancePerHttpRequest();

      builder.RegisterGeneric(typeof(EntityRepository<>))
             .As(typeof(IEntityRepository<>))
             .InstancePerHttpRequest();

      builder.RegisterType<ZADemoService>()
             .As<IZADemoService>()
             .InstancePerHttpRequest();

      return builder.Build();
    }
  }
}