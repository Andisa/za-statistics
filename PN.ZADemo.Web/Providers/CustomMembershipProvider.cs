﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using PN.ZADemo.Entity;
using PN.ZADemo.Service;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.EntityFramework;





namespace PN.ZADemo.Web.Providers
{
    public class CustomMembershipProvider : MembershipProvider
    {



       
    public CustomMembershipProvider() { }
   


        public override bool ValidateUser(string username, string password)
        {
            try
            {
                ZADemoEntities context = new ZADemoEntities();
                string pass = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");

                var user = context.Users.Where(u => u.UserName == username &&
                   u.PasswordHash == pass && u.IsActive == true).FirstOrDefault();
                if (user == null)
                    return false;

                else
                {
                    FormsAuthentication.SetAuthCookie(username, false);
                    return true;
                }




            }
            catch
            {
                return false;
            }
        }

        public override string ApplicationName
        {
            get { return "ZADemo"; }
            set { }
        }

        // Other overrides not implemented
        // Other overrides not implemented
        #region Other overrides not implemented
        public override MembershipUser CreateUser(string username, string password, string email,
    string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
    out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }


        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }



        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
