﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Security;
using PN.ZADemo.Entity;
using PN.ZADemo.Service;
using PN.ZADemo.Service.Interface;
using PN.ZADemo.EntityFramework;



namespace PN.ZADemo.Web.Providers
{
    public class CustomRoleProvider : RoleProvider
    {

       
    public CustomRoleProvider() { }
   

        public override string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();

            try
            {
                ZADemoEntities context = new ZADemoEntities();
                User user = context.Users.Include("Roles").Where(e => e.UserName == username).SingleOrDefault();

                if (user != null)
                {
                    //     The only way to find out what roles have been assigned to the user
                    //is to load the UsersRoles association


                    //Once populated any roles that the user has been assigned
                    //to should be available in the Roles property

                    foreach (Role role in user.Roles)
                        roles.Add(role.Role1);
                }
            }
            catch (Exception)
            {
            }

            return roles.ToArray();
        }

        public override string ApplicationName
        {
            get { return "ZA Demo "; }
            set { }
        }

        // Other overrides not implemented
        #region Not Implemented Overrides
        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
