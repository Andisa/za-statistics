﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PN.ZADemo.Web.Providers
{
    public static class Helper
    {
        public static string ShortMonth(int intMonth)
        {
            string mmm = "" ;
            switch (intMonth)
            {
                case 1:
                    mmm = "Jan";
                    break;
                case 2:
                    mmm = "Feb";
                    break;
                case 3:
                    mmm = "Mar";
                    break;
                case 4:
                    mmm = "Apr";
                    break;
                case 5:
                    mmm = "May";
                    break;
                case 6:
                    mmm = "Jun";
                    break;
                case 7:
                    mmm = "Jul";
                    break;
                case 8:
                    mmm = "Aug";
                    break;
                case 9:
                    mmm = "Sep";
                    break;
                case 10:
                    mmm = "Oct";
                    break;
                case 11:
                    mmm = "Nov";
                    break;
                case 12:
                    mmm = "Dec";
                    break;
            }
            return mmm;

        }


        public static string DecodeFrom64(string encodedData)
        {
            string result = "";
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(encodedData);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                result = new String(decoded_char);
            }
            catch
            {
            }
            return result;

        }

        public static string EncodeTo64(string toEncode)
        {
            string returnValue = "";
            try
            {
                byte[] toEncodeAsBytes
                      = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
                returnValue
                     = System.Convert.ToBase64String(toEncodeAsBytes);
            }
            catch
            {
            }
            return returnValue;
        }

        public static long DecoderLongID(string encodedData)
        {
            long id = 0;
            try
            {
                id = Convert.ToInt64(DecodeFrom64(encodedData));

            }
            catch
            {
            }
            return id;
        }

        public static long DecoderIntID(string encodedData)
        {
            int id = 0;
            try
            {
                id = Convert.ToInt32(DecodeFrom64(encodedData));

            }
            catch
            {
            }
            return id;
        }



    }
}