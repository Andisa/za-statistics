﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PN.ZADemo.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            System.Globalization.CultureInfo cultureInfo =
     new System.Globalization.CultureInfo("en-ZA");
            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");

            NumberFormatInfo oCurrentNFI = new NumberFormatInfo();

            oCurrentNFI.CurrencyGroupSeparator = " ";
            oCurrentNFI.CurrencyDecimalSeparator = ".";
            oCurrentNFI.CurrencyDecimalDigits = 2;
            oCurrentNFI.CurrencyNegativePattern = 1;
            oCurrentNFI.CurrencySymbol = "R";

            oCurrentNFI.NumberDecimalSeparator = ".";
            oCurrentNFI.NumberDecimalDigits = 2;

            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat = oCurrentNFI;
            System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat = oCurrentNFI;

            System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-ZA");
            System.Globalization.DateTimeFormatInfo dateTimeInfo =
 new System.Globalization.DateTimeFormatInfo();
            dateTimeInfo.LongDatePattern = "dd-MMM-yyyy";
            dateTimeInfo.ShortDatePattern = "dd-MMM-yyyy";
            cultureInfo.DateTimeFormat = dateTimeInfo;

            System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateTimeInfo;
            System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat = dateTimeInfo;
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            AutofacConfig.Initialize(GlobalConfiguration.Configuration);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
         
            AuthConfig.RegisterAuth();
        }
    }
}