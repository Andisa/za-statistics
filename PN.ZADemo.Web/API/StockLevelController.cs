﻿using BMA.SpeciePlanning.Business.Entities;
using BMA.SpeciePlanning.Business.Interfaces;
using BMA.SpeciePlanning.Client.Common;
using BMA.SpeciePlanning.EntityFramework;
using BMA.SpeciePlanning.Services.Abstract;
using BMA.SpeciePlanning.Web.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BMA.SpeciePlanning.Web.API
{
    public class StockLevelController : ApiController
    {
      private readonly ISpecieService _specieService;

      public StockLevelController(ISpecieService specieSvc) {
        _specieService = specieSvc;
      }

      public StockViewModel Get(long id, DateTime date) {

        //List<MaterialType> materialTypes = _speciePlanningService.GetMaterialTypes().ToList();
        List<MaterialType> materialTypes = _specieService.GetMaterialTypes().ToList();
        IEnumerable<CaptureValidation> captureValidations = _specieService.GetCaptureValidationsBySiteId(id);

        IEnumerable<Stock> stockList = _specieService.GetStocksByDate(date, id);
        IEnumerable<Stock> pStockList = _specieService.GetStocksForPreviousDate(date, id);

        StockViewModel viewModel = new StockViewModel();
        ResolveCurrentLevels(ref viewModel, stockList, materialTypes);
        ResolveStockValidations(ref viewModel, pStockList, materialTypes, captureValidations);
        

        return viewModel;

      }

      [HttpPost]
      public HttpResponseMessage Post(long id, DateTime date, StockViewModel model) {
        HttpResponseMessage response = Request.CreateResponse();

        List<MaterialType> materialTypes = _specieService.GetMaterialTypes().ToList();

        try {
          _specieService.AddOrUpdateStock(date, id,  model.ToStock(date, id, materialTypes));

          response.StatusCode = HttpStatusCode.OK;
        } catch {
          response.StatusCode = HttpStatusCode.InternalServerError;
        }

        return response;
      }

      private void ResolveStockValidations(ref StockViewModel stock, IEnumerable<Stock> pStockList, IEnumerable<MaterialType> materialTypes, IEnumerable<CaptureValidation> captureValidations) {
        if (pStockList.Count() > 0) {

          foreach (MaterialType mt in materialTypes) {
            double perChange = 10.0;
            decimal levelMin = 0;
            decimal levelMax = 0;

            CaptureValidation capValidation = captureValidations.FirstOrDefault(cv => cv.MaterialType_ID == mt.ID);
            if (capValidation != null) {
              perChange = capValidation.ValidPercChange;
            }

            Stock prevStockLevel = pStockList.FirstOrDefault(sl => sl.MaterialType_ID == mt.ID);
            if (prevStockLevel != null && prevStockLevel.Value.HasValue) {
              decimal change = prevStockLevel.Value.Value / (decimal)perChange;

              levelMin = Math.Round(prevStockLevel.Value.Value - change, 2);
              levelMax = Math.Round(prevStockLevel.Value.Value + change, 2);
            }

            switch (mt.SKUNumber.ToUpper()) {
              case "5C":
                stock.C5Min = levelMin; stock.C5Max = levelMax;
                break;
              case "10C":
                stock.C10Min = levelMin; stock.C10Max = levelMax;
                break;
              case "20C":
                stock.C20Min = levelMin; stock.C20Max = levelMax;
                break;
              case "50C":
                stock.C50Min = levelMin; stock.C50Max = levelMax;
                break;
              case "R1":
                stock.R1Min = levelMin; stock.R1Max = levelMax;
                break;
              case "R2":
                stock.R2Min = levelMin; stock.R2Max = levelMax;
                break;
              case "R5":
                stock.R5Min = levelMin; stock.R5Max = levelMax;
                break;
            }
          }
        }
      }

      private void ResolveCurrentLevels(ref StockViewModel stock, IEnumerable<Stock> stockList, IEnumerable<MaterialType> materialTypes) {
        if (stockList.Count() > 0) {

          foreach (MaterialType mt in materialTypes) {
            Stock stockLevel = stockList.FirstOrDefault(sl => sl.MaterialType_ID == mt.ID);
            if (stockLevel != null && stockLevel.Value.HasValue) {
              decimal val = Math.Round(stockLevel.Value.Value, 2);

              switch (mt.SKUNumber.ToUpper()) {
                case "5C":
                  stock.C5 = val;
                  break;
                case "10C":
                  stock.C10 = val;
                  break;
                case "20C":
                  stock.C20 = val;
                  break;
                case "50C":
                  stock.C50 = val;
                  break;
                case "R1":
                  stock.R1 = val;
                  break;
                case "R2":
                  stock.R2 = val;
                  break;
                case "R5":
                  stock.R5 = val;
                  break;
              }
            }
          }
        }
      }
    }
}
