﻿using BMA.SpeciePlanning.Business.Entities;
using BMA.SpeciePlanning.Business.Interfaces;
using BMA.SpeciePlanning.Client.Common;
using BMA.SpeciePlanning.Services.Abstract;
using BMA.SpeciePlanning.Web.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BMA.SpeciePlanning.Web.API
{
    public class BankSitesController : ApiController
    {
      private readonly ISpecieService _specieService;

      public BankSitesController(ISpecieService specieSvc) {
        this._specieService = specieSvc;
      }

      public IEnumerable<BankViewModel> Get() {

        IEnumerable<BankViewModel> banks = _specieService.GetBanks()
          .Select(bank => new BankViewModel () {
            Name = bank.BankName,
            Id = bank.ID,
            Sites = bank.Sites.Select(site => new BankSitesViewModel() {
              Id = site.ID,
              Name = site.Description
            })
          });

        return banks;
      }

      [HttpGet]
      [ActionName("MaterialTypes")]
      public string MaterialTypes() {

        var cv = _specieService.GetCaptureValidations().FirstOrDefault();

        if (cv.MaterialType != null) {
          return cv.MaterialType.SKUNumber;
        } else {
          return "oops.";
        }

      }

    }
}
