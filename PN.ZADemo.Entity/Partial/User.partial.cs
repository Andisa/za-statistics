﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PN.ZADemo.Entity
{
    public partial class User
    {
        private string strRoles;
        //[DataMember]
        // [Display(Name = "StrRoleID")]

        public string StrRoles
        {
            get
            {
                return strRoles;
            }
            set
            {
                strRoles = value;
            }
        }


        private string munID;
        //[DataMember]
        // [Display(Name = "StrRoleID")]

        public string MunID
        {
            get
            {
                return munID;
            }
            set
            {
                munID = value;
            }
        }

    }
}
