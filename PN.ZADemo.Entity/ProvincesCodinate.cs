//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PN.ZADemo.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProvincesCodinate
    {
        public ProvincesCodinate()
        {
            this.DistrictCordinates = new HashSet<DistrictCordinate>();
        }
    
        public int ID { get; set; }
        public string CODE { get; set; }
        public string PROVINCE { get; set; }
        public Nullable<float> Area { get; set; }
        public Nullable<float> Shape_Leng { get; set; }
        public Nullable<float> Shape_Area { get; set; }
        public System.Data.Spatial.DbGeometry geom { get; set; }
    
        public virtual ICollection<DistrictCordinate> DistrictCordinates { get; set; }
    }
}
